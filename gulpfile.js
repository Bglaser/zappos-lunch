var gulp = require('gulp');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var serverPort = 8000;

gulp.task('connect', function() {
  connect.server({
    root: './src',
    port: serverPort,
    livereload: true
  });
  console.log('[SERVER LISTENING] localhost:'+ serverPort);
});

gulp.task('html', function () {
  gulp.src('./src/*.html')
    .pipe(connect.reload());
});

gulp.task('sass', function () {
  gulp.src('./assets/sass/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./src/css'));
});

gulp.task('js', function() {
  gulp.src('./assets/js/*.js')
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./src/js'));
});

gulp.task('watch', function () {
  gulp.watch(['./assets/js/*.js', './src/*.html', './assets/sass/*.scss'], ['sass', 'js', 'html']);
});

gulp.task('default', ['connect', 'watch']);
